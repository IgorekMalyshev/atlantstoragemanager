use AtlantStorageManager 
go 

if object_id('dbo.Detail', 'U') is not null
	drop table dbo.Detail
go

create table dbo.Detail 
(
	DetailID			int				not null,
	NomenclatureCode	varchar(10)		not null,
	[Name]				varchar(20)		not null,	
	Amount				int,
	StoragekeeperID		int				not null,
	CreationDate		datetime		not null,
	DeletionDate		datetime
)
go