use AtlantStorageManager
go

if object_id('dbo.Storagekeeper', 'U') is not null
	drop table dbo.Storagekeeper
go

create table dbo.Storagekeeper 
(
	StoragekeeperID		int				not null,
	FullName			varchar(50)		not null
)
go