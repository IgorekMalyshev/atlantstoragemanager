use AtlantStorageManager
go

if object_id('FK_Detail_Storagekeeper', 'F') is not null
	alter table dbo.Detail drop constraint FK_Detail_Storagekeeper
go

alter table dbo.Detail 
	add constraint FK_Detail_Storagekeeper
	foreign key(StoragekeeperID)
	references dbo.Storagekeeper(StoragekeeperID)
	on delete cascade
go