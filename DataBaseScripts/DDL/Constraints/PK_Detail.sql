use AtlantStorageManager
go

if object_id('PK_Detail', 'PK') is not null
	alter table dbo.Detail drop constraint PK_Detail
go

alter table dbo.Detail 
	add constraint PK_Detail
	primary key(DetailID)
go