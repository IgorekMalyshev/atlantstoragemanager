use AtlantStorageManager
go

if object_id('PK_Storagekeeper', 'PK') is not null
	alter table dbo.Storagekeeper drop constraint PK_Storagekeeper
go

alter table dbo.Storagekeeper 
	add constraint PK_Storagekeeper
	primary key(StoragekeeperID)
go