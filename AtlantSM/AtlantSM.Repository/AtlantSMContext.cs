﻿using AtlantSM.Domain;
using AtlantSM.Repository.Mappings;
using Microsoft.EntityFrameworkCore;

namespace AtlantSM.Repository
{
    public class AtlantSMContext : DbContext
    {
        public DbSet<Detail> Details { get; set; }
        public DbSet<Storagekeeper> Storagekeepers { get; set; }

        public AtlantSMContext(DbContextOptions options)
            : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DetailMap());
            modelBuilder.ApplyConfiguration(new StoragekeeperMap());
        }
    }
}