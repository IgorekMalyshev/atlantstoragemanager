﻿using AtlantSM.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AtlantSM.Repository.Mappings
{
    public class DetailMap : IEntityTypeConfiguration<Detail>
    {
        public void Configure(EntityTypeBuilder<Detail> builder)
        {
            builder.ToTable("Detail", "dbo");
            builder.HasKey(hk => hk.DetailID);
            builder.Property(p => p.DetailID).IsRequired().HasColumnName("DetailID");
            builder.Property(p => p.NomenclatureCode).IsRequired().HasMaxLength(10).HasColumnName("NomenclatureCode");
            builder.Property(p => p.Name).IsRequired().HasMaxLength(20).HasColumnName("Name");
            builder.Property(p => p.Amount).HasColumnName("Amount");
            builder.Property(p => p.StoragekeeperID).IsRequired().HasColumnName("StoragekeeperID");
            builder.Property(p => p.CreationDate).HasColumnType("date").IsRequired().HasColumnName("CreationDate");
            builder.Property(p => p.DeletionDate).HasColumnType("date").HasColumnName("DeletionDate");

            builder.HasOne(sk => sk.Storagekeeper);
        }
    }
}