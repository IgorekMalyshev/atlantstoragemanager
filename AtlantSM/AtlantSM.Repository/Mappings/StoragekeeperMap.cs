﻿using AtlantSM.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AtlantSM.Repository.Mappings
{
    public class StoragekeeperMap : IEntityTypeConfiguration<Storagekeeper>
    {
        public void Configure(EntityTypeBuilder<Storagekeeper> builder)
        {
            builder.ToTable("Storagekeeper", "dbo");
            builder.HasKey(hk => hk.StoragekeeperID);
            builder.Property(p => p.StoragekeeperID).IsRequired().HasColumnName("StoragekeeperID");
            builder.Property(p => p.FullName).IsRequired().HasMaxLength(50).HasColumnName("FullName");
        }
    }
}