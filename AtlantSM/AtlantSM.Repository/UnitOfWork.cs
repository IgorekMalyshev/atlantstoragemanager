﻿using System;
using AtlantSM.IRepository;
using AtlantSM.IRepository.IRepositories;
using AtlantSM.Repository.Repositories;

namespace AtlantSM.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AtlantSMContext _context;
        private bool _disposed;
        private IDetailRepository _detailRepository;
        private IStoragekeeperRepository _storagekeeperRepository;

        public UnitOfWork(AtlantSMContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IDetailRepository DetailRepository =>
            _detailRepository ?? (_detailRepository = new EFDetailRepository(_context));

        public IStoragekeeperRepository StoragekeeperRepository =>
            _storagekeeperRepository ?? (_storagekeeperRepository = new EFStoragekeeperRepository(_context));
        
        public void Commit() => _context.SaveChanges();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _context.Dispose();
            }

            _disposed = true;
        }
    }
}