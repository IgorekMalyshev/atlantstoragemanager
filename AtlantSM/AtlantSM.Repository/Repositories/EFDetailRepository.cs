﻿using AtlantSM.Domain;
using AtlantSM.IRepository.IRepositories;

namespace AtlantSM.Repository.Repositories
{
    public class EFDetailRepository : EFEntityRepository<Detail>, IDetailRepository
    {
        public EFDetailRepository(AtlantSMContext context)
            : base(context) { }
    }
}