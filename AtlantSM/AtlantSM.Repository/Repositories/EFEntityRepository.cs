﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AtlantSM.IRepository.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace AtlantSM.Repository.Repositories
{
    public abstract class EFEntityRepository<TEntity> : IEntityRepository<TEntity> where TEntity : class
    {
        protected readonly AtlantSMContext Context;

        protected EFEntityRepository(AtlantSMContext context)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IEnumerable<TEntity> GetAll()
        {
            try
            {
                return Context.Set<TEntity>();
            }
            catch (Exception ex)
            {
                throw new RepositoryException("Something went wrong while trying to get collection of entities", ex);
            }
        }

        public IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> expression)
        {
            try
            {
                return Context.Set<TEntity>().Where(expression);
            }
            catch (Exception ex)
            {
                throw new RepositoryException("Something went wrong while trying to get collection of entities", ex);
            }
        }

        public TEntity Get(int id)
        {
            try
            {
                return Context.Find<TEntity>(id);
            }
            catch (Exception ex)
            {
                throw new RepositoryException("Something went wrong while trying to get entity", ex);
            }
        }

        public void Create(TEntity item)
        {
            try
            {
                Context.Add(item);
            }
            catch (Exception ex)
            {
                throw new RepositoryException("Something went wrong while trying to create entity", ex);
            }
        }

        public void Update(TEntity item)
        {
            try
            {
                Context.Entry(item).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                throw new RepositoryException("Something went wrong while trying to update entity", ex);
            }
        }

        public void Delete(int id)
        {
            try
            {
                var item = Context.Find<TEntity>(id);
                if (item != null)
                {
                    Context.Remove(item);
                }
            }
            catch (Exception ex)
            {
                throw new RepositoryException("Something went wrong while trying to delete entity", ex);
            }
        }
    }
}