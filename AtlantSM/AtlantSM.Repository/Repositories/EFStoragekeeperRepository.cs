﻿using AtlantSM.Domain;
using AtlantSM.IRepository.IRepositories;

namespace AtlantSM.Repository.Repositories
{
    public class EFStoragekeeperRepository : EFEntityRepository<Storagekeeper>, IStoragekeeperRepository
    {
        public EFStoragekeeperRepository(AtlantSMContext context)
            : base(context) { }
    }
}