﻿using System;
using System.Collections.Generic;
using System.Text;
using AtlantSM.Domain;

namespace AtlantSM.IRepository.IRepositories
{
    public interface IStoragekeeperRepository : IEntityRepository<Storagekeeper> { }
}
