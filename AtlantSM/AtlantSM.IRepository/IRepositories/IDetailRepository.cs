﻿using AtlantSM.Domain;

namespace AtlantSM.IRepository.IRepositories
{
    public interface IDetailRepository : IEntityRepository<Detail> { }
}
