﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace AtlantSM.IRepository.IRepositories
{
    public interface IEntityRepository<TEntity>
    {
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetAll(Expression<Func<TEntity,bool>> expression);
        TEntity Get(int id);
        void Create(TEntity item);
        void Update(TEntity item);
        void Delete(int id);
    }
}
