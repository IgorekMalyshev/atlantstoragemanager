﻿using AtlantSM.IRepository.IRepositories;

namespace AtlantSM.IRepository
{
    public interface IUnitOfWork
    {
        IDetailRepository DetailRepository { get; }
        IStoragekeeperRepository StoragekeeperRepository { get; }

        void Commit();
    }
}