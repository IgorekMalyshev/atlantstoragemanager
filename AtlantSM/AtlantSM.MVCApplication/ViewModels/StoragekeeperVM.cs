﻿using System.ComponentModel.DataAnnotations;

namespace AtlantSM.MVCApplication.ViewModels
{
    public class StoragekeeperVM
    {
        public int StoragekeeperID { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [StringLength(50,ErrorMessage = "Full name maximum length should be 50 characters")]
        public string FullName { get; set; }
    }
}