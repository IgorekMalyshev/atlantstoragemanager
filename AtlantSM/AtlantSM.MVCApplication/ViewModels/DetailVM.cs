﻿using System;
using System.ComponentModel.DataAnnotations;
using AtlantSM.Domain;

namespace AtlantSM.MVCApplication.ViewModels
{
    public class DetailVM
    {
        public int DetailID { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [StringLength(50, ErrorMessage = "Nomenclature code maximum length should be 10 characters")]
        public string NomenclatureCode { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [StringLength(50, ErrorMessage = "Name maximum length should be 20 characters")]
        public string Name { get; set; }

        public int Amount { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public int StoragekeeperID { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public DateTime CreationDate { get; set; }

        public DateTime DeletionDate { get; set; }
        public Storagekeeper Storagekeeper { get; set; }
    }
}