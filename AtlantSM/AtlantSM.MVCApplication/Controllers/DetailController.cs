﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AtlantSM.Domain;
using AtlantSM.IServices;
using AtlantSM.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace AtlantSM.MVCApplication.Controllers
{
    [ApplicationExceptionFilter]
    [Route("api/details")]
    public class DetailController : Controller
    {
        private readonly IDetailService _detailService;
        public DetailController(IDetailService detailService)
        {
            _detailService =
                detailService ?? throw new ArgumentNullException(nameof(detailService));
        }


        [HttpGet]
        public IEnumerable<Detail> Get()
        {
            return _detailService.GetDetails().ToList();
        }

        [HttpGet("(DetailID)")]
        public Detail GetDetail(int detailID)
        {
            return _detailService.GetDetail(detailID);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Detail detail)
        {
            if (ModelState.IsValid)
            {
                _detailService.CreateDetail(detail);
                return Ok(detail);
            }

            return BadRequest(ModelState);
        }

        [HttpPut("{DetailID}")]
        public IActionResult Put([FromBody] Detail detail)
        {
            if (ModelState.IsValid)
            {
                _detailService.EditDetail(detail);
                return Ok(detail);
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{DetailID}")]
        public IActionResult Delete(int detailID)
        {
            var detail = _detailService.GetDetail(detailID);
            if (detail != null)
            {
                _detailService.DeleteDetail(detailID);
            }

            return Ok(detail);
        }
    }
}