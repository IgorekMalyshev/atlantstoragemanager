﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AtlantSM.Domain;
using AtlantSM.IServices;
using AtlantSM.Services;
using Microsoft.AspNetCore.Mvc;

namespace AtlantSM.MVCApplication.Controllers
{
    [ApplicationExceptionFilter]
    [Route("api/storagekeeper")]
    public class StoragekeeperController : Controller
    {
        private readonly IStoragekeeperService _storagekeeperService;
        public StoragekeeperController(IStoragekeeperService storagekeeperService)
        {
            _storagekeeperService =
                storagekeeperService ?? throw new ArgumentNullException(nameof(storagekeeperService));
        }

        [HttpGet]
        public IEnumerable<Storagekeeper> Get()
        {
            return _storagekeeperService.GetStoragekeepers().ToList();
        }

        [HttpGet("(StoragekeeperID)")]
        public Storagekeeper GetStoragekeeper(int storagekeeperID)
        {
            return _storagekeeperService.GetStoragekeeper(storagekeeperID);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Storagekeeper storagekeeper)
        {
            if (ModelState.IsValid)
            {
                _storagekeeperService.CreateStoragekeeper(storagekeeper);
                return Ok(storagekeeper);
            }

            return BadRequest(ModelState);
        }

        [HttpPut("{StoragekeeperID}")]
        public IActionResult Put([FromBody] Storagekeeper storagekeeper)
        {
            if (ModelState.IsValid)
            {
                _storagekeeperService.EditStoragekeeper(storagekeeper);
                return Ok(storagekeeper);
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{StoragekeeperID}")]
        public IActionResult Delete(int storagekeeperID)
        {
            bool hasDependants;
            var storagekeeper = _storagekeeperService.GetStoragekeeper(storagekeeperID);
            if (storagekeeper != null)
            {
                _storagekeeperService.DeleteStoragekeeper(storagekeeperID, out hasDependants);
                //проверить hasDependants == true? если да ,то вывести пользователю уведомление(как? - подумать)
            }

            return Ok(storagekeeper);
        }
    }
}