﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AtlantSM.IRepository;
using AtlantSM.IServices;
using AtlantSM.Repository;
using AtlantSM.Services;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AtlantSM.MVCApplication
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddDbContext<AtlantSMContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("AtlantSMConnection")));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IDetailService, DetailService>();
            services.AddScoped<IStoragekeeperService, StoragekeeperService>();
            services.AddAutoMapper(typeof(Startup));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }

            app.UseDefaultFiles();
            app.UseStatusCodePagesWithRedirects("/Error/{0}");
            app.UseStaticFiles();
            app.UseMvc(routes => { });
        }

    }
}
