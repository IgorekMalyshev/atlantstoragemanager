﻿using AtlantSM.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace AtlantSM.MVCApplication
{
    public class ApplicationExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            switch (context.Exception)
            {
                case ServiceException _:
                    context.Result = new ViewResult()
                    {
                        ViewName = "Error",
                        ViewData = new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary())
                        {
                            Model = "Oops, some service related error occured..."
                        }
                    };
                    break;
                 default:
                        context.Result = new ViewResult()
                        {
                            ViewName = "Error",
                            ViewData = new ViewDataDictionary(new EmptyModelMetadataProvider(),
                                new ModelStateDictionary())
                            {
                                Model = "Oops, some error occured..."
                            }
                        };
                        break;
            }
        }
    }
}