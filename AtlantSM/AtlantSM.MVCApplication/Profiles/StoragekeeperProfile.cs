﻿using AtlantSM.Domain;
using AtlantSM.MVCApplication.ViewModels;
using AutoMapper;

namespace AtlantSM.MVCApplication.Profiles
{
    public class StoragekeeperProfile : Profile
    {
        public StoragekeeperProfile()
        {
            CreateMap<Storagekeeper, StoragekeeperVM>();
            CreateMap<StoragekeeperVM, Storagekeeper>();
        }
    }
}