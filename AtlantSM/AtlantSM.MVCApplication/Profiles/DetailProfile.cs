﻿using AtlantSM.Domain;
using AtlantSM.MVCApplication.ViewModels;
using AutoMapper;

namespace AtlantSM.MVCApplication.Profiles
{
    public class DetailProfile : Profile
    {
        public DetailProfile()
        {
            CreateMap<Detail, DetailVM>();
            CreateMap<DetailVM, Detail>();
        }
    }
}