﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Detail } from './detail';

@Injectable()
export class DataService {

    private url = "/api/details";

    constructor(private http: HttpClient) {
    }

    getDetails() {
        return this.http.get(this.url);
    }

    createDetail(detail: Detail) {
        return this.http.post(this.url, detail);
    }

    updateDetail(detail: Detail) {
        return this.http.put(this.url + '/' + detail.DetailID, detail);
    }

    deleteDetail(detailID: number) {
        return this.http.delete(this.url + '/' + detailID)
    }
}