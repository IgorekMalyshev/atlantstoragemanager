﻿export class Detail {
    constructor(
        public DetailID?: number,
        public NomenclatureCode?: string, 
        public Name?: string,
        public Amount?: number,
        public StoragekeeperID?: number,
        public CreationDate?: string, 
        public DeletionDate?: string) {}
}