﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storagekeeper } from './storagekeeper';


@Injectable()
export class DataService {

    private url = "/api/storagekeeper";

    constructor(private http: HttpClient) {
    }

    getStoragekeepers() {
        return this.http.get(this.url);
    }

    createStoragekeeper(storagekeeper: Storagekeeper) {
        return this.http.post(this.url, storagekeeper);
    }

    updateStoragekeeper(storagekeeper: Storagekeeper) {
        return this.http.put(this.url + '/' + storagekeeper.StoragekeeperID, storagekeeper);
    }

    deleteStoragekeeper(storagekeeperID: number) {
        return this.http.delete(this.url + '/' + storagekeeperID)
    }
}