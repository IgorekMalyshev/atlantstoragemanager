﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using AtlantSM.Domain;
using AtlantSM.IRepository;
using AtlantSM.IServices;

namespace AtlantSM.Services
{
    public class StoragekeeperService : Service, IStoragekeeperService
    {
        public StoragekeeperService(IUnitOfWork unitOfWork)
            : base(unitOfWork) { }

        public IEnumerable<Storagekeeper> GetStoragekeepers()
        {
            try
            {
                return UnitOfWork.StoragekeeperRepository.GetAll();
            }
            catch (Exception ex)
            {
                throw new ServiceException("Something went wrong while trying to get storagekeepers",ex);
            }
        }

        public IEnumerable<Storagekeeper> GetStoragekeepers(Expression<Func<Storagekeeper, bool>> expression)
        {
            try
            {
                return UnitOfWork.StoragekeeperRepository.GetAll(expression);
            }
            catch (Exception ex)
            {
                throw new ServiceException("Something went wrong while trying to get storagekeepers", ex);
            }
        }

        public Storagekeeper GetStoragekeeper(int storagekeeperID)
        {
            try
            {
                return UnitOfWork.StoragekeeperRepository.Get(storagekeeperID);
            }
            catch (Exception ex)
            {
                throw new ServiceException("Something went wrong while trying to get storagekeeper", ex);
            }
        }

        public void CreateStoragekeeper(Storagekeeper storagekeeper)
        {
            try
            {
                UnitOfWork.StoragekeeperRepository.Create(storagekeeper);
                UnitOfWork.Commit();
            }
            catch (Exception ex)
            {
                throw new ServiceException("Something went wrong while trying to create a storagekeeper", ex);
            }
        }

        public void EditStoragekeeper(Storagekeeper storagekeeper)
        {
            try
            {
                UnitOfWork.StoragekeeperRepository.Update(storagekeeper);
                UnitOfWork.Commit();
            }
            catch (Exception ex)
            {
                throw new ServiceException("Something went wrong while trying to edit storagekeeper", ex);
            }
        }

        public void DeleteStoragekeeper(int storagekeeperID, out bool hasDependants)
        {
            try
            {
                var detail = UnitOfWork.DetailRepository.GetAll(details => details.StoragekeeperID == storagekeeperID)
                    ?.FirstOrDefault();
                if (detail == null)
                {
                    hasDependants = false;
                    UnitOfWork.StoragekeeperRepository.Delete(storagekeeperID);
                }
                else
                {
                    hasDependants = true;
                }
            }
            catch (Exception ex)
            {
                throw new ServiceException("Something went wrong while trying to delete storagekeeper", ex);
            }
        }
    }
}
