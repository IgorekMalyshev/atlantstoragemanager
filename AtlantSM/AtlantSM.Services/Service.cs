﻿using System;
using System.Collections.Generic;
using System.Text;
using AtlantSM.IRepository;

namespace AtlantSM.Services
{
    public abstract class Service
    {
        protected readonly IUnitOfWork UnitOfWork;

        protected Service(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }
    }
}
