﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using AtlantSM.Domain;
using AtlantSM.IRepository;
using AtlantSM.IServices;

namespace AtlantSM.Services
{
    public class DetailService : Service, IDetailService
    {
        public DetailService(IUnitOfWork unitOfWork)
            : base(unitOfWork) { }

        public IEnumerable<Detail> GetDetails()
        {
            try
            {
                return UnitOfWork.DetailRepository.GetAll();
            }
            catch (Exception ex)
            {
                throw new ServiceException("Something went wrong while trying to get details", ex);
            }
        }

        public IEnumerable<Detail> GetDetails(Expression<Func<Detail, bool>> expression)
        {
            try
            {
                return UnitOfWork.DetailRepository.GetAll(expression);
            }
            catch (Exception ex)
            {
                throw new ServiceException("Something went wrong while trying to get details", ex);
            }
        }

        public Detail GetDetail(int detailID)
        {
            try
            {
                return UnitOfWork.DetailRepository.Get(detailID);
            }
            catch (Exception ex)
            {
                throw new ServiceException("Something went wrong while trying to get detail", ex);
            }
        }

        public void CreateDetail(Detail detail)
        {
            try
            {
                UnitOfWork.DetailRepository.Create(detail);
                UnitOfWork.Commit();
            }
            catch (Exception ex)
            {
                throw new ServiceException("Something went wrong while trying to create a detail", ex);
            }
        }

        public void EditDetail(Detail detail)
        {
            try
            {
                UnitOfWork.DetailRepository.Update(detail);
                UnitOfWork.Commit();
            }
            catch (Exception ex)
            {
                throw new ServiceException("Something went wrong while trying to edit detail", ex);
            }
        }

        public void DeleteDetail(int detailID)
        {
            try
            {
                UnitOfWork.DetailRepository.Delete(detailID);
            }
            catch (Exception ex)
            {
                throw new ServiceException("Something went wrong while trying to delete detail", ex);
            }
        }
    }
}
