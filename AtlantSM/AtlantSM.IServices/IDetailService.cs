﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AtlantSM.Domain;

namespace AtlantSM.IServices
{

    public interface IDetailService
    {
        IEnumerable<Detail> GetDetails();
        IEnumerable<Detail> GetDetails(Expression<Func<Detail, bool>> expression);
        Detail GetDetail(int detailID);
        void CreateDetail(Detail detail);
        void EditDetail(Detail detail);
        void DeleteDetail(int detailID);
    }
}