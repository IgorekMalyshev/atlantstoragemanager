﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AtlantSM.Domain;

namespace AtlantSM.IServices
{
    public interface IStoragekeeperService
    {
        IEnumerable<Storagekeeper> GetStoragekeepers();
        IEnumerable<Storagekeeper> GetStoragekeepers(Expression<Func<Storagekeeper, bool>> expression);
        Storagekeeper GetStoragekeeper(int storagekeeperID);
        void CreateStoragekeeper(Storagekeeper storagekeeper);
        void EditStoragekeeper(Storagekeeper storagekeeper);
        void DeleteStoragekeeper(int storagekeeperID, out bool hasDependants);
    }
}