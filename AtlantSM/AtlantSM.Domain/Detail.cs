﻿using System;

namespace AtlantSM.Domain
{
    public class Detail
    {
        public int DetailID { get; set; }
        public string NomenclatureCode { get; set; }
        public string Name { get; set; }
        public int Amount { get; set; }
        public int StoragekeeperID { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime DeletionDate { get; set; }

        public virtual Storagekeeper Storagekeeper { get; set; }
    }
}
