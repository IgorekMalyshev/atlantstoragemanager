﻿CREATE TABLE [dbo].[Detail] (
    [DetailID]         INT          NOT NULL,
    [NomenclatureCode] VARCHAR (10) NOT NULL,
	[Name]			   VARCHAR (20) NOT NULL,
    [Amount]           INT          NULL,
    [StoragekeeperID]  INT          NOT NULL,
    [CreationDate]     DATETIME     NOT NULL,
    [DeletionDate]     DATETIME     NULL,
    CONSTRAINT [PK_Detail] PRIMARY KEY CLUSTERED ([DetailID] ASC),
    CONSTRAINT [FK_Detail_Storagekeeper] FOREIGN KEY ([StoragekeeperID]) REFERENCES [dbo].[Storagekeeper] ([StoragekeeperID]) ON DELETE CASCADE
);

