﻿CREATE TABLE [dbo].[Storagekeeper] (
    [StoragekeeperID] INT          NOT NULL,
    [FullName]        VARCHAR (50) NULL,
    CONSTRAINT [PK_Storagekeeper] PRIMARY KEY CLUSTERED ([StoragekeeperID] ASC)
);

