﻿
use AtlantStorageManager
GO

delete from dbo.Storagekeeper
GO

insert into dbo.Storagekeeper(StoragekeeperID, FullName)
values
	(1, 'Золотов Василий Иванович '),
	(2, 'Красовский Андрей Геннадьевич'),
	(3, 'Милько Антон Вадимович')
GO

delete from dbo.Detail
GO

insert into dbo.Detail(DetailID, NomenclatureCode, [Name], Amount, StoragekeeperID, CreationDate, DeletionDate)
values
	(1, 'A34GH5', 'Двигатель', 30, 2, '2019-04-06', null),
	(2, 'FF54HG', 'Дверь', 50, 2, '2019-04-08', null),
	(3, 'FF76T5', 'Розетка', 40, 1, '2019-05-09', '2019-06-13'),
	(4, 'HH3L85', 'Ножка', 100, 1, '2019-05-13', null),
	(5, 'KFP76F', 'Полка', 20, 2, '2019-03-15', '2019-05-11'),
	(6, 'DERT57', 'Полка двойная', 150, 3, '2019-04-16', null),
	(7, 'RTLC34', 'Ручка', 39, 3, '2019-04-17', '2019-05-01'),
	(8, 'RT40R3', 'Лампа', 100, 1, '2019-05-02', null)
GO
